<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipo_de_Sangre extends Model
{
    use SoftDeletes;
    
    protected $table = 'tipos_de_sangre';

    protected $dates = ['deleted_at'];
}
