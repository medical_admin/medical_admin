<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Historial_Medico extends Model
{
    use SoftDeletes;
    
    protected $table = 'historiales_medicos';

    protected $fillable = [
        'descripcion',
        'peso',
        'altura',
        'fecha',
        'tipo',
        'medicamentos',
        'paciente_id'
    ];

    protected $dates = ['deleted_at'];

    public function paciente(){
        return $this->belongsTo('App\Paciente');
    }
}
