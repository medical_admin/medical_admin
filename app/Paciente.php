<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paciente extends Model
{
    use SoftDeletes;
    
    protected $table = 'pacientes';

    protected $fillable = [
        'nombre',
        'apellidos',
        'fecha_de_nacimiento',
        'alergias',
        'genero_id',
        'tipo_de_sangre_id'
    ];

    protected $dates = ['deleted_at'];

    public function genero(){
        return $this->belongsTo('App\Genero');
    }

    public function tipo_de_sangre(){
        return $this->belongsTo('App\Tipo_de_Sangre');
    }

    public function historiales_medicos(){
        return $this->hasMany('App\Historial_Medico');
    }

}
