<?php

namespace App\Http\Controllers;

use App\Tipo_de_Sangre;
use App\Genero;
use App\Paciente;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PacienteController extends Controller
{
    public function index(){

        $pacientes = Paciente::withTrashed()->paginate(10);

        return view('pacientes.index')->with('pacientes', $pacientes);
    }


    public function create(){

        $generos = Genero::all();
        $tipos_de_sangre = Tipo_de_Sangre::all();
        return view('pacientes.create', ['generos'=> $generos, 'tipos_de_sangre'=> $tipos_de_sangre]);
   }

   public function store(Request $request){
       $paciente = new Paciente;
       $paciente->nombre = $request->nombre;
       $paciente->apellidos = $request->apellidos;
       $paciente->alergias = $request->alergias;
       $paciente->fecha_de_nacimiento = $request->fecha_de_nacimiento;
       $paciente->genero_id = $request->genero;
       $paciente->tipo_de_sangre_id = $request->tipo_de_sangre;

       $paciente->save();

       return redirect(action('PacienteController@index'));
   }

   public function show($id){
       $paciente = Paciente::withTrashed()->find($id);
       return view('pacientes.show', ['paciente'=>$paciente]);
   }

   public function edit($id){

        $paciente = Paciente::withTrashed()->find($id);
        $generos = Genero::all();
        $tipos_de_sangre = Tipo_de_Sangre::all();

        return view('pacientes.edit', ['generos'=> $generos, 'tipos_de_sangre'=> $tipos_de_sangre,
                                        'paciente' => $paciente]);
    }

    public function update(Request $request, $id){

        $paciente = Paciente::find($id);
        $paciente->nombre = $request->nombre;
        $paciente->apellidos = $request->apellidos;
        $paciente->alergias = $request->alergias;
        $paciente->fecha_de_nacimiento = $request->fecha_de_nacimiento;
        $paciente->genero_id = $request->genero;
        $paciente->tipo_de_sangre_id = $request->tipo_de_sangre;

        $paciente->save();

        return view('pacientes.show', ['paciente'=>$paciente]);
    }

    public function habilita($id){
        $paciente = Paciente::withTrashed()->find($id);
        $paciente->restore();
        $paciente->save();
        return redirect()->back();
    }

   public function destroy($id){

       $paciente = Paciente::find($id);
       $paciente->delete();

       return redirect()->back();
   }
}
