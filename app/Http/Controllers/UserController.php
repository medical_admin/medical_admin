<?php

namespace App\Http\Controllers;

use App\Rol;
use Session;
use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;

class UserController extends Controller
{
    public function index(){
        // $user = Auth::user();
        // if ($user->cannot('create', User::class)) {
        //     abort(404);
        // }

        $users = User::withTrashed()->paginate(10);

        return view('usuarios.index')->with('users', $users);
    }


    public function create(){
        // $user = Auth::user();
        // if ($user->cannot('create',User::class)) {
        //     abort(404);
        // }

        $roles = Rol::all();
        return view('usuarios.create', ['roles'=>$roles]);
   }

   public function store(Request $request){
    //    $user = Auth::user();
    //    if ($user->canbot('create', User::class)) {
    //        abort(404);
    //    }
       $user = new User;
       $user->nombre = $request->nombre;
       $user->apellidos = $request->apellidos;
       $user->email = $request->email;
       $user->fecha_de_nacimiento = $request->fecha_de_nacimiento;
       $user->password = bcrypt($request->password);
       $user->rol_id = $request->rol;
       $user->save();

       return redirect(action('UserController@index'));
   }

   public function show($id){
       $user = User::withTrashed()->find($id);
       return view('usuarios.show', ['user'=>$user]);
   }

   public function edit($id){
        // get the user
        // $user = Auth::user();
        // if ($user->cannot('update', User::withTrashed()->find($id))) {
        //    abort(404);
        // }

        $user = User::withTrashed()->find($id);
        $roles = Rol::all();
        // show the edit form and pass the user
        return view('usuarios.edit', ['user'=> $user,'roles'=>$roles]);
    }

    public function update(Request $request, $id){
        // $user = Auth::user();
        // if ($user->cannot('update',  User::find($id))) {
        //    abort(404);
        // }
        $user = User::find($id);

        $user->nombre = $request->nombre;
        $user->apellidos = $request->apellidos;
        $user->email = $request->email;
        if($request->edit_password != ''){
            $user->password      = bcrypt($request->edit_password);
        }

        $user->fecha_de_nacimiento = $request->fecha_de_nacimiento;

        // if(Auth::user()->rol_id == 1){
        if($user->rol_id != $request->rol){

          if($request->rol != $user->rol_id){
              $user->rol()->dissociate();
              $user->rol()->associate($request->rol);
            }

        }

        $user->save();

        return view('usuarios.show', ['user'=>$user]);
    }

    public function habilita($id){
        $user = User::withTrashed()->find($id);
        $user->restore();
        $user->save();
        return redirect()->back();
    }

   public function destroy($id){
    //    $user = Auth::user();
    //    if ($user->cannot('delete', User::find($id))) {
    //        abort(404);
    //    }

       $user = User::find($id);
       $user->delete();

       return redirect()->back();
   }

}
