<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table){
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->date('fecha_de_nacimiento');
            $table->longText('alergias');

            $table->integer('genero_id')->unsigned();
            $table->foreign('genero_id')->references('id')->on('generos');

            $table->integer('tipo_de_sangre_id')->unsigned();
            $table->foreign('tipo_de_sangre_id')->references('id')->on('tipos_de_sangre');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
