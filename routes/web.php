<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::resource('user', 'UserController');
Route::any('user/react/{id}', 'UserController@habilita')->name('userR');
Route::any('user/desact/{id}', 'UserController@destroy')->name('userD');

Route::resource('patient', 'PacienteController');
Route::any('patient/react/{id}', 'PacienteController@habilita')->name('patientR');
Route::any('patient/desact/{id}', 'PacienteController@destroy')->name('patientD');
