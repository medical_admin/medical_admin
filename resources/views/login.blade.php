
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Login Example - Semantic</title>

  <link rel="stylesheet" type="text/css" href="/css/semantic-ui/semantic.min.css">
  <link rel="stylesheet" type="text/css" href="/css/semantic-ui/components/icon.min.css">
  <script src="/js/jquery-3.2.0.min.js"></script>
  <script src="/js/semantic-ui/semantic.min.js"></script>

  <style type="text/css">
    body {
      background-color: #FFFFFF;
    }
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            email: {
              identifier  : 'email',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Por favor ingrese su e-mail'
                },
                {
                  type   : 'email',
                  prompt : 'Por favor ingrese un e-mail valido'
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Por favor ingrese su contraseña'
                },
                {
                  type   : 'length[6]',
                  prompt : 'Su contraseña debe se mayor a 6 carácteres'
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
</head>
<body>

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <img src="/images/logo.png" class="image">
    <br>
    <h2 class="ui teal image header">
      <div class="content">
        Ingrese a su cuenta
      </div>
    </h2>
    <form class="ui large form">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input type="text" name="email" placeholder="Correo electrónico">
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" placeholder="Contraseña">
          </div>
        </div>
        <div class="ui fluid large teal submit button" onclick=window.location.href="/patient/">Iniciar Sesión</div>
      </div>

      <div class="ui error message"></div>

    </form>

  </div>
</div>

</body>

</html>
