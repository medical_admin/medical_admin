@extends('dashboard')

@section('scripts')
    <link rel="stylesheet" type="text/css" href="/css/semantic-ui/components/button.min.css">
    <script type="text/javascript" src="/js/semantic-ui/semantic.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/transition.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/form.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/modal.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/dropdown.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        $(document).ready(function(){
            $('.ui.dropdown').dropdown({
                forceSelection: false,
            });

            $( '#datepicker' ).datepicker({

              altField: "#datepicker",
              altFormat: "yy-mm-dd",
              changeMonth: true,
              changeYear: true,
              minDate: "-100Y",
              maxDate: new Date()

          });

            $('.ui.form').form({
              inline : true,
              on: 'blur',
              fields: {
                nombre: {
                  identifier  : 'nombre',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Nombre necesario'
                  }]
                },
                apellidos: {
                  identifier  : 'apellidos',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Apellidos necesarios'
                  }]
                },
                alergias: {
                  identifier  : 'alergias',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Sección de alergias necesaria'
                  }]
                },
                fecha_de_nacimiento: {
                  identifier  : 'fecha_de_nacimiento',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Fecha necesaria'
                  }]
                },
                genero: {
                  identifier  : 'genero',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Genero necesario'
                  }]
                },
                tipos_de_sangre: {
                  identifier  : 'tipos_de_sangre',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Tipo de sangre necesaria'
                  }]
                }
              }
            });

        });

        function registrarModal(){
            if ($('.ui.form').form('is valid')) {
                $('.small.modal').modal('show');
            }
        }
    </script>
@endsection

@section('contenido')
    <div class="ui two column stackable grid">
        <div class="thirteen wide computer eleven wide tablet column center aligned">
            <h2 class="ui header" style="margin-top:30px;">Registrar Paciente</h2>
        </div>

        <div class="three wide computer five wide tablet column">
            <div class="ui horizontal segments">

                <div class="ui segment center aligned" data-tooltip="Guardar Paciente" data-position="bottom center" onclick="registrarModal()">
                    <h3><i class="edit icon"></i></h3>
                </div>

                <div class="ui segment center aligned" onclick=window.location.href="{{action('PacienteController@index')}}" data-tooltip="Regresar" data-position="bottom center">
                    <h3><i class="arrow circle outline left icon"></i></h3>
                </div>
            </div>
        </div>
    </div>


    <form class="ui form" action="{{action('PacienteController@store')}}" method="POST" enctype="multipart/form-data" id="Registrar">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="ui three column stackable grid">
            <div class="one wide computer zero wide tablet zero wide mobile column"></div>

            <div class="thirteen wide computer sixteen wide tablet sixteen wide mobile column">

                <div class="two fields">
                    <div class="field">
                        <label>Nombre </label>
                        <input type="text" value="{{old('nombre')}}" name="nombre" placeholder="Nombre"/>
                    </div>

                    <div class="field">
                        <label>Apellidos</label>
                        <input type="text" value="{{old('apellidos')}}" name="apellidos" placeholder="Apellidos"/>
                    </div>

                </div>

                <div class="two fields">

                    <div class="field">
                        <label>Alergías</label>
                        <input type="text" value="{{old('alergias')}}" name="alergias" placeholder="Alergía al Mani" />
                    </div>

                    <div class="field">
                        <label>Género</label>
                            <select name="genero" class="ui selection dropdown" >
                                <option value="{{old('genero')}}">Seleccione un género</option>
                                @foreach($generos as $genero)
                                    <option  value="{{$genero->id}}">{{$genero->nombre}}</option>
                                @endforeach
                            </select>
                    </div>

                </div>

                <div class="two fields">

                    <div class="field">
                        <label>Fecha de nacimiento</label>
                        <input type="text" id="datepicker" value="{{old('fecha_de_nacimiento')}}" name="fecha_de_nacimiento" placeholder="yyyy-mm-dd">
                    </div>

                    <div class="field">
                        <label>Tipos de Sangre</label>
                            <select name="tipo_de_sangre" class="ui selection dropdown" >
                                <option value="{{old('tipo_de_sangre')}}">Selecciona un tipo de sangre</option>
                                @foreach($tipos_de_sangre as $tipo_de_sangre)
                                    <option  value="{{$tipo_de_sangre->id}}">{{$tipo_de_sangre->nombre}}</option>
                                @endforeach
                            </select>
                    </div>


                </div>

            </div>

        </div>

        <div class="ui small modal">
            <div class="header">Registrar Paciente</div>
            <div class="content">
                <div class="description">
                    <p>¿Seguro deseas registrar el nuevo paciente?</p>
                </div>
            </div>
            <div class="actions">
                <a class="negative ui button">Cancelar</a>
                <button class="positive submit ui button" type="submit" form="Registrar" value="registrar">
                Registrar</button>
            </div>
        </div>


    </form>


@endsection
