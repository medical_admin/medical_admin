@extends('dashboard')

@section('scripts')
    <script type="text/javascript" src="/js/semantic-ui/components/transition.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/dropdown.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/modal.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/form.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/semantic.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <script type="text/javascript">

      $(document).ready(function(){
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown({
            forceSelection: false,
        });
    });


    </script>
@endsection

@section('contenido')
    <div class="ui four column stackable grid">
        <div class="twelve wide computer eleven wide tablet column center aligned">
            <h2 class="ui header" style="margin-top:30px;">Paciente {{$paciente->nombre}} {{$paciente->apellidos}}</h2>
        </div>

        <div class="four wide computer five wide tablet column">
            <div class="ui horizontal segments">

                <div class="ui segment center aligned"  data-tooltip="Actualizar Paciente" data-position="bottom center">
                    <h3><a href="{{action('PacienteController@edit' , $paciente->id)}}"><i class="edit icon"></i></a></i></h3>
                </div>

                @if($paciente->deleted_at == NULL)
                    <div class="ui segment center aligned" data-tooltip="Deshabilitar Usuario" data-position="bottom center">
                        <a href="{{route('patientD' , $paciente->id)}}"><i class="red lock icon"></i></a>
                    </div>
                @else
                    <div class="ui segment center aligned" data-tooltip="Habilitar Usuario" data-position="bottom center">
                        <a href="{{route('patientR' , $paciente->id)}}"><i class=" green unlock icon"></i></a>
                    </div>
                @endif

                <div class="ui segment center aligned" onclick=window.location.href="/patient/" data-tooltip="Regresar" data-position="bottom center">
                    <h3><i class="arrow circle outline left icon"></i></h3>
                </div>
            </div>
        </div>
    </div>

    <form class="ui form" action="{{route('patient.update', $paciente->id)}}" method="POST" enctype="multipart/form-data" id="Actualizar">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="ui three column stackable grid">
            <div class="one wide computer zero wide tablet zero wide mobile column"></div>

            <div class="thirteen wide computer sixteen wide tablet sixteen wide mobile column">

                <div class="ui segments">
                    <div class="ui horizontal segments">
                        <div class="ui segment">
                            <h4>Nombre:</h4><p>{{$paciente->nombre}}</p>
                        </div>
                        <div class="ui segment">
                            <h4>Apellidos:</h4><p>{{$paciente->apellidos}}</p>
                        </div>
                        <div class="ui segment">
                            <h4>Alergías</h4><p>{{$paciente->alergias}}</p>
                        </div>
                    </div>
                    <div class="ui horizontal segments">
                        <div class="ui segment">
                            <h4>Fecha de Nacimiento:</h4><p>{{$paciente->fecha_de_nacimiento}}</p>
                        </div>
                        <div class="ui segment">
                            <h4>Género:</h4><p>{{$paciente->genero->nombre}}</p>
                        </div>
                        <div class="ui segment">
                            <h4>Tipo de Sangre:</h4><p>{{$paciente->tipo_de_sangre->nombre}}</p>
                        </div>
                        <div class="ui segment">
                            <h4>Paciente:</h4>
                            <p>
                                @if($paciente->deleted_at == NULL)
                                Activo
                                @else
                                Inactivo
                                @endif
                            </p>
                        </div>
                    </div>
              </div>

            </div>

        </div>

@endsection
