@extends('dashboard')

@section('scripts')
    <link rel="stylesheet" type="text/css" href="/css/semantic-ui/components/button.min.css">
    <script type="text/javascript" src="/js/semantic-ui/semantic.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/transition.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/form.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/modal.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/dropdown.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <script type="text/javascript">

      $(document).ready(function(){
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown({
            forceSelection: false,
        });

        $( '#datepicker' ).datepicker({

          altField: "#datepicker",
          altFormat: "yy-mm-dd",
          changeMonth: true,
          changeYear: true,
          minDate: "-100Y",
          maxDate: new Date()

      });

      $('.ui.form').form({
        inline : true,
        on: 'blur',
        fields: {
          nombre: {
            identifier  : 'nombre',
            rules: [{
              type   : 'empty',
              prompt : 'Nombre necesario'
            }]
          },
          apellidos: {
            identifier  : 'apellidos',
            rules: [{
              type   : 'empty',
              prompt : 'Apellidos necesarios'
            }]
          },
          alergias: {
            identifier  : 'alergias',
            rules: [{
              type   : 'empty',
              prompt : 'Sección de alergias necesaria'
            }]
          },
          fecha_de_nacimiento: {
            identifier  : 'fecha_de_nacimiento',
            rules: [{
              type   : 'empty',
              prompt : 'Fecha necesaria'
            }]
          },
          genero: {
            identifier  : 'genero',
            rules: [{
              type   : 'empty',
              prompt : 'Genero necesario'
            }]
          },
          tipos_de_sangre: {
            identifier  : 'tipos_de_sangre',
            rules: [{
              type   : 'empty',
              prompt : 'Tipo de sangre necesaria'
            }]
          }
        }
      });

        $('#msgtamano').hide();
        $('#msgextension').hide();
        $('#input').removeClass('error');

      });

      function updateModal() {
        if ($('.ui.form').form('is valid')) {
            $('.small.modal').modal('show');
        }
      }

    </script>
@endsection

@section('contenido')
    <div class="ui four column stackable grid">
        <div class="twelve wide computer eleven wide tablet column center aligned">
            <h2 class="ui header" style="margin-top:30px;">Editar Paciente</h2>
        </div>

        <div class="four wide computer five wide tablet column">
            <div class="ui horizontal segments">

                <div class="ui segment center aligned" onclick="updateModal()" data-tooltip="Actualizar Paciente" data-position="bottom center">
                    <h3><i class="save icon"></i></h3>
                </div>

                @if($paciente->deleted_at == NULL)
                    <div class="ui segment center aligned" data-tooltip="Deshabilitar Paciente" data-position="bottom center">
                        <a href="{{route('patientD' , $paciente->id)}}"><i class="red lock icon"></i></a>
                    </div>
                @else
                    <div class="ui segment center aligned" data-tooltip="Habilitar Paciente" data-position="bottom center">
                        <a href="{{route('patientR' , $paciente->id)}}"><i class=" green unlock icon"></i></a>
                    </div>
                @endif

                <div class="ui segment center aligned" onclick=onclick=window.location.href="/patient/" data-tooltip="Regresar" data-position="bottom center">
                    <h3><i class="arrow circle outline left icon"></i></h3>
                </div>
            </div>
        </div>
    </div>

    <form class="ui form" action="{{route('patient.update', $paciente->id)}}" method="POST" enctype="multipart/form-data" id="Actualizar">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="ui three column stackable grid">
            <div class="one wide computer zero wide tablet zero wide mobile column"></div>

            <div class="thirteen wide computer sixteen wide tablet sixteen wide mobile column">

                <div class="two fields">
                    @if($paciente->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Nombre </label>
                        <input type="text" value="{{$paciente->nombre}}" name="nombre" placeholder="Nombre"/>
                    </div>

                    @if($paciente->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Apellidos</label>
                        <input type="text" value="{{$paciente->apellidos}}" name="apellidos" placeholder="Apellidos"/>
                    </div>

                </div>

                <div class="two fields">

                    @if($paciente->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Alergias</label>
                        <input type="text" value="{{$paciente->alergias}}" name="alergias" placeholder="Alergía al maní" />
                    </div>

                    @if($paciente->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Género</label>
                            <select name="genero" class="ui selection dropdown" >
                                <option value="{{old('tipo_de_sangre')}}">Seleccione un género</option>
                                @foreach($generos as $genero)
                                    @if($paciente->genero_id == $genero->id)
                                        <option  value="{{$genero->id}}" selected="true">{{$genero->nombre}}</option>
                                    @else
                                        <option  value="{{$genero->id}}">{{$genero->nombre}}</option>
                                    @endif
                                @endforeach
                            </select>
                    </div>


                </div>

                <div class="two fields">

                    @if($paciente->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Fecha de nacimiento</label>
                        <input type="text" id="datepicker" value="{{$paciente->fecha_de_nacimiento}}" name="fecha_de_nacimiento" placeholder="yyyy-mm-dd">
                    </div>

                    @if($paciente->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Tipos de Sangre</label>
                            <select name="tipo_de_sangre" class="ui selection dropdown" >
                                <option value="{{old('tipo_de_sangre')}}">Selecciona un tipo de sangre</option>
                                @foreach($tipos_de_sangre as $tipo_de_sangre)
                                    @if($paciente->tipo_de_sangre_id == $tipo_de_sangre->id)
                                        <option  value="{{$tipo_de_sangre->id}}" selected="true">{{$tipo_de_sangre->nombre}}</option>
                                    @else
                                        <option  value="{{$tipo_de_sangre->id}}">{{$tipo_de_sangre->nombre}}</option>
                                    @endif
                                @endforeach
                            </select>
                    </div>


                </div>

            </div>

        </div>

        <div class="ui small modal">
            <div class="header">Actualizar Paciente</div>
            <div class="content">
                <div class="description">
                    <p>¿Seguro deseas actualizar el paciente?</p>
                </div>
            </div>
            <div class="actions">
                <a class="negative ui button">Cancelar</a>
                <button class="positive submit ui button" type="submit" form="Actualizar" value="actualizar">
                Actualizar</button>
            </div>
        </div>

    </form>

@endsection
