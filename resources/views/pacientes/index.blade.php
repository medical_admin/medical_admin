@extends('dashboard')

@section('scripts')
    <script type="text/javascript" src="/js/semantic-ui/components/transition.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/form.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/modal.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/sortT.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/semantic.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
          $('table').tablesort();
          $('.ui.form').form();
          $('.ui.modal').modal();
        });
        </script>


@endsection

@section('contenido')
    <div class="ui four column stackable grid">
        <div class="fourteen wide computer twelve wide tablet column">
            <h2 class="ui header" style="margin-top:30px; margin-bottom:30px;">Paciente</h2>
        </div>

        <div class="two wide computer four wide tablet column">
            <div class="ui horizontal segments">

                <div class="ui segment center aligned" onclick=window.location.href="{{action('PacienteController@create')}}" data-tooltip="Crear Paciente" data-position="bottom center">
                    <h3><i class="add user icon"></i></h3>
                </div>
            </div>
        </div>


    </div>

    <div class="ui row">

            <table class="ui fixed sortable celled table">
                <thead>
                    <tr>
                        <th class="three wide sorted descending center aligned">Nombre</th>
                        <th class="three wide sorted descending center aligned">Apellidos</th>
                        <th class="three wide sorted descending center aligned">Tipo de Sangre</th>
                        <th class="two wide sorted descendingd center aligned">Género</th>
                        <th class="two wide center aligned" colspan="3">Opciones</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($pacientes as $paciente)
                        @if($paciente->deleted_at != NULL)
                            <tr class="negative">
                        @else
                            <tr class="">
                        @endif

                        <td>{{$paciente->nombre}}</td>
                        <td>{{$paciente->apellidos}}</td>
                        <td>{{$paciente->tipo_de_sangre->nombre}}</td>
                        <td>{{$paciente->genero->nombre}}</td>
                        <td class="center aligned selectable">
                            <a href="{{action('PacienteController@show' , $paciente->id)}}"><i class="info circle icon"></i></a>
                        </td>
                        <td class="center aligned selectable">
                            <a href="{{action('PacienteController@edit' , $paciente->id)}}"><i class="edit icon"></i></a>
                        </td>

                        @if($paciente->deleted_at == NULL)
                            <td class="center aligned selectable">
                                <a href="{{route('patientD' , $paciente->id)}}"><i class="red lock icon"></i></a>
                            </td>
                        @else
                            <td class="center aligned selectable">
                                <a href="{{route('patientR' , $paciente->id)}}"><i class=" green unlock icon"></i></a>
                            </td>
                        @endif

                        </tr>
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <th colspan="7">
                            <div class="ui right floated pagination menu">
                            <a href="{{$pacientes->previousPageUrl()}}" class="icon item">
                                <i class="left chevron icon"></i>
                            </a>

                            @for($i = 1; $i <= $pacientes->lastPage(); $i++)
                                <a href="{{$pacientes->url($i)}}" class="item"> {{$i}} </a>
                            @endfor

                            <a href="{{$pacientes->nextPageUrl()}}" class="icon item">
                                <i class="right chevron icon"></i>
                            </a>
                            </div>
                        </th>
                    </tr>
                </tfoot>

            </table>

        </div>


@endsection
