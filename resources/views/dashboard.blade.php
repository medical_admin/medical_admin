<?php
use Illuminate\Support\Facades\Auth;
use App\User;
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- Standard Meta -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <link rel="icon" href="/images/logo.png" type="image/png" sizes="16x16">

        <!-- Site Properties -->
        <title>Medical Admin</title>
        <link rel="stylesheet" type="text/css" href="/css/semantic-ui/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="/css/semantic-ui/components/icon.min.css">
        <script src="/js/jquery-3.2.0.min.js"></script>
        @yield('scripts')
    </head>

    <body data-gr-c-s-loaded="true">

        <div class="ui two column stackable grid">
            <div class="three wide column">
                <div class="ui secondary vertical pointing fluid menu">
                    <div class="item">
                        <img class="ui image" src="/images/logo.png">
                    </div>
                    <div class="item">
                        <h4 class="ui header">
                            <i class="user icon"></i>
                            Armando Martínez Mandujano
                        </h4>
                    </div>

                    <div class="ui fitted divider"></div>

                    <a class="item active" href="/dashboard">
                        <i class="calendar icon"></i>Agenda</a>

                    <a class="item active" href="/patient">
                        <i class="child icon"></i>Pacientes</a>

                    <a class="item active" href="/user">
                        <i class="users icon"></i>Usuarios</a>
                    <a class="item active" href="/">
                        <i class="sign in icon"></i>Cerrar Sesión</a>

                </div>
            </div>
            <div class="twelve wide column">
                @yield('contenido')
            </div>

        </div>

    </body>
</html>
