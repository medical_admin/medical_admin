@extends('dashboard')

@section('scripts')
    <link rel="stylesheet" type="text/css" href="/css/semantic-ui/components/button.min.css">
    <script type="text/javascript" src="/js/semantic-ui/semantic.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/transition.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/form.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/modal.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/dropdown.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        $(document).ready(function(){
            $('.ui.dropdown').dropdown({
                forceSelection: false,
            });

            $( '#datepicker' ).datepicker({

              altField: "#datepicker",
              altFormat: "yy-mm-dd",
              changeMonth: true,
              changeYear: true,
              minDate: "-100Y",
              maxDate: new Date()

          });

            $('.ui.form').form({
              inline : true,
              on: 'blur',
              fields: {
                nombre: {
                  identifier  : 'nombre',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Nombre necesario'
                  }]
                },
                apellidos: {
                  identifier  : 'apellidos',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Apellidos necesarios'
                  }]
                },
                email: {
                  identifier  : 'email',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Correo Electrónico necesario'
                  }]
                },
                fecha_de_nacimiento: {
                  identifier  : 'fecha_de_nacimiento',
                  rules: [{
                    type   : 'empty',
                    prompt : 'Fecha necesaria'
                  }]
                }
              }
            });

        });

        function registrarModal(){
            if ($('.ui.form').form('is valid')) {
                $('.small.modal').modal('show');
            }
        }
    </script>
@endsection

@section('contenido')
    <div class="ui two column stackable grid">
        <div class="thirteen wide computer eleven wide tablet column center aligned">
            <h2 class="ui header" style="margin-top:30px;">Registrar Usuario</h2>
        </div>

        <div class="three wide computer five wide tablet column">
            <div class="ui horizontal segments">

                <div class="ui segment center aligned" data-tooltip="Guardar Usuario" data-position="bottom center" onclick="registrarModal()">
                    <h3><i class="edit icon"></i></h3>
                </div>

                <div class="ui segment center aligned" onclick=window.location.href="{{action('UserController@index')}}" data-tooltip="Regresar" data-position="bottom center">
                    <h3><i class="arrow circle outline left icon"></i></h3>
                </div>
            </div>
        </div>
    </div>


    <form class="ui form" action="{{action('UserController@store')}}" method="POST" enctype="multipart/form-data" id="Registrar">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="ui three column stackable grid">
            <div class="one wide computer zero wide tablet zero wide mobile column"></div>

            <div class="thirteen wide computer sixteen wide tablet sixteen wide mobile column">

                <div class="two fields">
                    <div class="field">
                        <label>Nombre </label>
                        <input type="text" value="{{old('nombre')}}" name="nombre" placeholder="Nombre"/>
                    </div>

                    <div class="field">
                        <label>Apellidos</label>
                        <input type="text" value="{{old('apellidos')}}" name="apellidos" placeholder="Apellidos"/>
                    </div>

                </div>

                <div class="three fields">

                    <div class="field">
                        <label>Correo Electrónico</label>
                        <input type="email" value="{{old('email')}}" name="email" placeholder="ejemplo@ejemplo.com" />
                    </div>

                    <div class="field">
                        <label>Contraseña</label>
                        <input type="password" placeholder="min 8 carácteres" name="password" />
                    </div>

                    <div class="field">
                        <label>Verificar Contraseña</label>
                        <input type="password" placeholder="min 8 carácteres" name="v_password" />
                    </div>
                </div>

                <div class="two fields">

                    <div class="field">
                        <label>Fecha de nacimiento</label>
                        <input type="text" id="datepicker" value="{{old('fecha_de_nacimiento')}}" name="fecha_de_nacimiento" placeholder="yyyy-mm-dd">
                    </div>

                    <div class="field">
                        <label>Rol</label>
                            <select name="rol" class="ui selection dropdown" >
                                <option value="{{old('rol')}}">Selecciona un rol</option>
                                @foreach($roles as $rol)
                                    <option  value="{{$rol->id}}">{{$rol->nombre}}</option>
                                @endforeach
                            </select>
                    </div>


                </div>

            </div>

        </div>

        <div class="ui small modal">
            <div class="header">Registrar Usuario</div>
            <div class="content">
                <div class="description">
                    <p>¿Seguro deseas registrar el nuevo usuario?</p>
                </div>
            </div>
            <div class="actions">
                <a class="negative ui button">Cancelar</a>
                <button class="positive submit ui button" type="submit" form="Registrar" value="registrar">
                Registrar</button>
            </div>
        </div>
        <div class="ui error message"></div>
        @if(count($errors) > 0)
        <div class="ui negative message">
            <ul class="ui list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

    </form>


@endsection
