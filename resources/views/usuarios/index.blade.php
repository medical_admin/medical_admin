@extends('dashboard')

@section('scripts')
    <script type="text/javascript" src="/js/semantic-ui/components/transition.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/form.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/modal.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/sortT.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/semantic.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
          $('table').tablesort();
          $('.ui.form').form();
          $('.ui.modal').modal();
        });
        </script>


@endsection

@section('contenido')
    <div class="ui four column stackable grid">
        <div class="fourteen wide computer twelve wide tablet column">
            <h2 class="ui header" style="margin-top:30px; margin-bottom:30px;">Usuarios</h2>
        </div>

        <div class="two wide computer four wide tablet column">
            <div class="ui horizontal segments">

                <div class="ui segment center aligned" onclick=window.location.href="{{action('UserController@create')}}" data-tooltip="Crear Usuario" data-position="bottom center">
                    <h3><i class="add user icon"></i></h3>
                </div>
            </div>
        </div>


    </div>

    <div class="ui row">

            <table class="ui fixed sortable celled table">
                <thead>
                    <tr>
                        <th class="three wide sorted descending center aligned">Nombre</th>
                        <th class="three wide sorted descending center aligned">Apellidos</th>
                        <th class="three wide sorted descending center aligned">Correo Eletrónico</th>
                        <th class="two wide sorted descendingd center aligned">Rol de Usuario</th>
                        <th class="two wide center aligned" colspan="3">Opciones</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($users as $user)
                        @if($user->deleted_at != NULL)
                            <tr class="negative">
                        @else
                            <tr class="">
                        @endif

                        <td>{{$user->nombre}}</td>
                        <td>{{$user->apellidos}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->rol->nombre}}</td>
                        <td class="center aligned selectable">
                            <a href="{{action('UserController@show' , $user->id)}}"><i class="info circle icon"></i></a>
                        </td>
                        <td class="center aligned selectable">
                            <a href="{{action('UserController@edit' , $user->id)}}"><i class="edit icon"></i></a>
                        </td>

                        @if($user->deleted_at == NULL)
                            <td class="center aligned selectable">
                                <a href="{{route('userD' , $user->id)}}"><i class="red lock icon"></i></a>
                            </td>
                        @else
                            <td class="center aligned selectable">
                                <a href="{{route('userR' , $user->id)}}"><i class=" green unlock icon"></i></a>
                            </td>
                        @endif

                        </tr>
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <th colspan="7">
                            <div class="ui right floated pagination menu">
                            <a href="{{$users->previousPageUrl()}}" class="icon item">
                                <i class="left chevron icon"></i>
                            </a>

                            @for($i = 1; $i <= $users->lastPage(); $i++)
                                <a href="{{$users->url($i)}}" class="item"> {{$i}} </a>
                            @endfor

                            <a href="{{$users->nextPageUrl()}}" class="icon item">
                                <i class="right chevron icon"></i>
                            </a>
                            </div>
                        </th>
                    </tr>
                </tfoot>

            </table>

        </div>


@endsection
