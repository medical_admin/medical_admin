@extends('dashboard')

@section('scripts')
    <link rel="stylesheet" type="text/css" href="/css/semantic-ui/components/button.min.css">
    <script type="text/javascript" src="/js/semantic-ui/semantic.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/transition.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/form.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/modal.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/dropdown.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <script type="text/javascript">

      $(document).ready(function(){
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown({
            forceSelection: false,
        });

        $( '#datepicker' ).datepicker({

          altField: "#datepicker",
          altFormat: "yy-mm-dd",
          changeMonth: true,
          changeYear: true,
          minDate: "-100Y",
          maxDate: new Date()

      });

        $('.ui.form').form({
          inline : true,
          on: 'blur',
          fields: {
            nombre: {
              identifier  : 'nombre',
              rules: [{
                type   : 'empty',
                prompt : 'Nombre necesario'
              }]
            },
            apellidos: {
              identifier  : 'apellidos',
              rules: [{
                type   : 'empty',
                prompt : 'Apellidos necesarios'
              }]
            },
            email: {
              identifier  : 'email',
              rules: [{
                type   : 'empty',
                prompt : 'Correo Electrónico necesario'
              }]
            },
            fecha_de_nacimiento: {
              identifier  : 'fecha_de_nacimiento',
              rules: [{
                type   : 'empty',
                prompt : 'Fecha necesaria'
              }]
            }
          }
        });

        $('#msgtamano').hide();
        $('#msgextension').hide();
        $('#input').removeClass('error');

      });

      function updateModal() {
        if ($('.ui.form').form('is valid')) {
            $('.small.modal').modal('show');
        }
      }

    </script>
@endsection

@section('contenido')
    <div class="ui four column stackable grid">
        <div class="twelve wide computer eleven wide tablet column center aligned">
            <h2 class="ui header" style="margin-top:30px;">Editar Usuario</h2>
        </div>

        <div class="four wide computer five wide tablet column">
            <div class="ui horizontal segments">

                <div class="ui segment center aligned" onclick="updateModal()" data-tooltip="Actualizar Usuario" data-position="bottom center">
                    <h3><i class="save icon"></i></h3>
                </div>

                @if($user->deleted_at == NULL)
                    <div class="ui segment center aligned" data-tooltip="Deshabilitar Usuario" data-position="bottom center">
                        <a href="{{route('userD' , $user->id)}}"><i class="red lock icon"></i></a>
                    </div>
                @else
                    <div class="ui segment center aligned" data-tooltip="Habilitar Usuario" data-position="bottom center">
                        <a href="{{route('userR' , $user->id)}}"><i class=" green unlock icon"></i></a>
                    </div>
                @endif

                <div class="ui segment center aligned" onclick=onclick=window.location.href="/user/" data-tooltip="Regresar" data-position="bottom center">
                    <h3><i class="arrow circle outline left icon"></i></h3>
                </div>
            </div>
        </div>
    </div>

    <form class="ui form" action="{{route('user.update', $user->id)}}" method="POST" enctype="multipart/form-data" id="Actualizar">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="ui three column stackable grid">
            <div class="one wide computer zero wide tablet zero wide mobile column"></div>

            <div class="thirteen wide computer sixteen wide tablet sixteen wide mobile column">

                <div class="two fields">
                    @if($user->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Nombre </label>
                        <input type="text" value="{{$user->nombre}}" name="nombre" placeholder="Nombre"/>
                    </div>

                    @if($user->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Apellidos</label>
                        <input type="text" value="{{$user->apellidos}}" name="apellidos" placeholder="Apellidos"/>
                    </div>

                </div>

                <div class="three fields">

                    @if($user->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Correo Electrónico</label>
                        <input type="email" value="{{$user->email}}" name="email" placeholder="ejemplo@ejemplo.com" />
                    </div>

                    @if($user->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Contraseña</label>
                        <input type="password" placeholder="min 8 carácteres" name="password" />
                    </div>

                    @if($user->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Verificar Contraseña</label>
                        <input type="password" placeholder="min 8 carácteres" name="v_password" />
                    </div>
                </div>

                <div class="two fields">

                    @if($user->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Fecha de nacimiento</label>
                        <input type="text" id="datepicker" value="{{$user->fecha_de_nacimiento}}" name="fecha_de_nacimiento" placeholder="yyyy-mm-dd">
                    </div>

                    @if($user->deleted_at != NULL)
                        <div class="disabled field">
                    @else
                        <div class="field">
                    @endif
                        <label>Rol</label>
                            <select name="rol" class="ui selection dropdown" >
                                <option value="{{old('rol')}}">Selecciona un rol</option>
                                @foreach($roles as $rol)
                                    @if($user->rol_id == $rol->id)
                                        <option  value="{{$rol->id}}" selected="true">{{$rol->nombre}}</option>
                                    @else
                                        <option  value="{{$rol->id}}">{{$rol->nombre}}</option>
                                    @endif
                                @endforeach
                            </select>
                    </div>


                </div>

            </div>

        </div>

        <div class="ui small modal">
            <div class="header">Actualizar Usuario</div>
            <div class="content">
                <div class="description">
                    <p>¿Seguro deseas actualizar el usuario?</p>
                </div>
            </div>
            <div class="actions">
                <a class="negative ui button">Cancelar</a>
                <button class="positive submit ui button" type="submit" form="Actualizar" value="actualizar">
                Actualizar</button>
            </div>
        </div>
        <div class="ui error message"></div>
        @if(count($errors) > 0)
        <div class="ui negative message">
            <ul class="ui list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

    </form>

@endsection
