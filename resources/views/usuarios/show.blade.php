@extends('dashboard')

@section('scripts')
    <script type="text/javascript" src="/js/semantic-ui/components/transition.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/dropdown.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/modal.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/components/form.min.js"></script>
    <script type="text/javascript" src="/js/semantic-ui/semantic.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <script type="text/javascript">

      $(document).ready(function(){
        $('.ui.modal').modal();
        $('.ui.dropdown').dropdown({
            forceSelection: false,
        });
    });


    </script>
@endsection

@section('contenido')
    <div class="ui four column stackable grid">
        <div class="twelve wide computer eleven wide tablet column center aligned">
            <h2 class="ui header" style="margin-top:30px;">Usuario {{$user->nombre}} {{$user->apellidos}}</h2>
        </div>

        <div class="four wide computer five wide tablet column">
            <div class="ui horizontal segments">

                <div class="ui segment center aligned"  data-tooltip="Actualizar Usuario" data-position="bottom center">
                    <h3<a href="{{action('UserController@edit' , $user->id)}}"><i class="edit icon"></i></a></i></h3>
                </div>

                @if($user->deleted_at == NULL)
                    <div class="ui segment center aligned" data-tooltip="Deshabilitar Usuario" data-position="bottom center">
                        <a href="{{route('userD' , $user->id)}}"><i class="red lock icon"></i></a>
                    </div>
                @else
                    <div class="ui segment center aligned" data-tooltip="Habilitar Usuario" data-position="bottom center">
                        <a href="{{route('userR' , $user->id)}}"><i class=" green unlock icon"></i></a>
                    </div>
                @endif

                <div class="ui segment center aligned" onclick=onclick=window.location.href="/user/" data-tooltip="Regresar" data-position="bottom center">
                    <h3><i class="arrow circle outline left icon"></i></h3>
                </div>
            </div>
        </div>
    </div>

    <form class="ui form" action="{{route('user.update', $user->id)}}" method="POST" enctype="multipart/form-data" id="Actualizar">
    <input type="hidden" name="_method" value="PATCH">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="ui three column stackable grid">
            <div class="one wide computer zero wide tablet zero wide mobile column"></div>

            <div class="thirteen wide computer sixteen wide tablet sixteen wide mobile column">

                <div class="ui segments">
                    <div class="ui horizontal segments">
                        <div class="ui segment">
                            <h4>Nombre:</h4><p>{{$user->nombre}}</p>
                        </div>
                        <div class="ui segment">
                            <h4>Apellidos:</h4><p>{{$user->apellidos}}</p>
                        </div>
                        <div class="ui segment">
                            <h4>Correo electrónico</h4><p>{{$user->email}}</p>
                        </div>
                    </div>
                    <div class="ui horizontal segments">
                        <div class="ui segment">
                            <h4>Fecha de Nacimiento:</h4><p>{{$user->fecha_de_nacimiento}}</p>
                        </div>
                        <div class="ui segment">
                            <h4>Rol de Usuario:</h4><p>{{$user->rol->nombre}}</p>
                        </div>
                        <div class="ui segment">
                            <h4>Usuario:</h4>
                            <p>
                                @if($user->deleted_at == NULL)
                                Activo
                                @else
                                Inactivo
                                @endif
                            </p>
                        </div>
                    </div>
              </div>

            </div>

        </div>

@endsection
